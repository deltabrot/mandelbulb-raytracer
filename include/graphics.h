#ifndef __GRAPHICS_H__
#define __GRAPHICS_H__

#include <SDL.h>

void drawPixel(SDL_Surface*, int, int, Uint32);

#endif
