#ifndef __VEC_H__
#define __VEC_H__

#define PI 3.14159265
#define NaN 999999999999999

#include <math.h>

typedef struct {
    float x;
    float y;
    float z;
} Vec3f;

Vec3f addVec(Vec3f, Vec3f);
Vec3f subtractVec(Vec3f, Vec3f);
Vec3f multiplyVec(float, Vec3f);
Vec3f unitVec(Vec3f v);
Vec3f powerVec(Vec3f v, float exponent);
float modulusVec(Vec3f v);
float polarVec(Vec3f v);
float azimuthVec(Vec3f v);

#endif
