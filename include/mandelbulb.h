#ifndef __MANDELBULB_H__
#define __MANDELBULB_H__

#include <SDL.h>
#include <stdbool.h>
#include "vec.h"
#include "color.h"
#include "graphics.h"

typedef struct {
    float size;
    float distance;
    Vec3f eye;
} Bulb;

void renderBulb(Bulb, SDL_Window*, SDL_Surface*, int, int, bool);

#endif
