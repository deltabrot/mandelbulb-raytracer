CC = gcc
BUILDDIR = build
BINDIR = bin
SRCDIR = src
CFLAGS = -I/usr/include/SDL2 -D_REENTRANT -lm
INC = -L/usr/lib -lSDL2 -Iinclude
TARGET = $(BINDIR)/output
OBJECTS = $(shell find $(SRCDIR) -name "*.c" | sed -r "s|$(SRCDIR)/([a-zA-Z]+).c|$(BUILDDIR)/\1.o|")

default: main

main: $(OBJECTS)
	@mkdir -p $(BINDIR)
	$(CC) $(CFLAGS) $(INC) -o $(TARGET) $^

$(BUILDDIR)/%.o: $(SRCDIR)/%.c
	@mkdir -p $(BUILDDIR)
	$(CC) $(CFLAGS) $(INC) -c $< -o $@

clean:
	@echo "Removing $(BUILDDIR)/"; rm -r $(BUILDDIR)
