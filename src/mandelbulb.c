#include "mandelbulb.h"
#include "color.h"
#include "graphics.h"

void renderBulb(Bulb bulb, SDL_Window* window, SDL_Surface* surface, int WIDTH, int HEIGHT, bool runOnce){
    int offsetX = WIDTH/2;
    int offsetY = HEIGHT/2;
    for(int y=-offsetY; y<HEIGHT-offsetY; y++){
        for(int x=-offsetX; x<WIDTH-offsetX; x++){
            Vec3f imagePlane = {x*bulb.size/WIDTH, y*bulb.size/HEIGHT, bulb.distance};
            Vec3f imagePlaneUnit = unitVec(subtractVec(imagePlane, bulb.eye));
            float stepSize = 0.001;

            Vec3f v0 = {0.0, 0.0, 0.0};
            int stepNum = 5000;
            for(int step = 0; step < stepNum; step++){
                Vec3f c = addVec(imagePlane, multiplyVec(stepSize*step, imagePlaneUnit));
                Vec3f zn = addVec(powerVec(v0, 8), c);
                bool escaped = false;
                for(int i=0;i<100;i++){
                    zn = addVec(powerVec(zn,8), c);

                    if(modulusVec(zn) > 2){
                        escaped = true;
                        break;
                    }
                }
                if(!escaped){
                    //int colorShade = (int)(((float)step/(float)stepNum)*255); // Black & White
                    drawPixel(surface, x+offsetX, y+offsetY, heatStepHex(step));
                    break;
                }
            }
        }
        if(!runOnce){
            SDL_UpdateWindowSurface(window);
        }
        printf("%d/%d\n", (y + offsetY), HEIGHT);
    }        
}
