#include "graphics.h"

void drawPixel(SDL_Surface *surface, int x, int y, Uint32 pixel) {
  Uint32 *target_pixel = (Uint32*) ((Uint8 *) surface->pixels + y * surface->pitch + x * sizeof *target_pixel);
  *target_pixel = pixel;
}

