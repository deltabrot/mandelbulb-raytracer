#include "vec.h"

Vec3f addVec(Vec3f v1, Vec3f v2){
    return (Vec3f){v1.x+v2.x, v1.y+v2.y, v1.z+v2.z};
}

Vec3f subtractVec(Vec3f v1, Vec3f v2){
    return (Vec3f){v1.x-v2.x, v1.y-v2.y, v1.z-v2.z};
}

Vec3f multiplyVec(float scalar, Vec3f v){
    return (Vec3f){scalar*v.x, scalar*v.y, scalar*v.z};
}

Vec3f unitVec(Vec3f v){
    float length = modulusVec(v);
    return multiplyVec(1.0/length, v);
}

Vec3f powerVec(Vec3f v, float exponent){
    float modExp = pow(modulusVec(v), exponent);
    float polar = polarVec(v);
    float azimuth = azimuthVec(v);
    float polarExp = exponent*polar;
    float azimuthExp = exponent*azimuth;
    float sinPolarExp = sin(polarExp);
    Vec3f trigVec = (Vec3f){sinPolarExp*cos(azimuthExp), sinPolarExp*sin(azimuthExp), cos(polarExp)};
    return multiplyVec(modExp, trigVec);
}

float modulusVec(Vec3f v){
    return sqrt(pow(v.x,2) + pow(v.y,2) + pow(v.z,2));
}

float polarVec(Vec3f v){
    if(v.x != 0) {
        return atan(v.y/v.x);
    } else {
        return PI/2.0;
    }
}

float azimuthVec(Vec3f v){
    if(v.z != 0) {
        return atan(sqrt(pow(v.x,2) + pow(v.y,2))/v.z);
    } else {
        return PI/2.0;
    }
}
