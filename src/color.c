#include "color.h"

int rgbHex(int r, int g, int b){
    if( r < 0 ){
        r = 0;
    }
    if( r > 255 ){
        r = 255;
    }
    if( g < 0 ){
        g = 0;
    }
    if( g > 255 ){
        g = 255;
    }
    if( b < 0 ){
        b = 0;
    }
    if( b > 255 ){
        b = 255;
    }
    return (r*65536) + (g*256) + (b);
}

int heatStepHex(int step){
    int R = 255;
    int G = 0;
    int B = 0;
    int cycle = (step%1536);
    if(cycle < 256){
        G = cycle%256;
    }
    else if(cycle < 512){
        R = 255 - (cycle%256);
        G = 255;
    }
    else if(cycle < 768){
        R = 0;
        G = 255;
        B = cycle%256;
    }
    else if(cycle < 1024){
        R = 0;
        G = 255 - (cycle%256);
        B = 255;
    }
    else if(cycle < 1280){
        R = cycle%256;
        B = 255;
    }
    else{
        B = 255 - (cycle%256);
    }
    return rgbHex(R,G,B);
}
