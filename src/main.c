#include <SDL.h>
#include <stdio.h>
#include <stdbool.h>
#include "vec.h"
#include "color.h"
#include "graphics.h"
#include "mandelbulb.h"

const int WIDTH = 100;
const int HEIGHT = 100;

SDL_Window* window = NULL;
SDL_Surface* surface = NULL;

bool initSDL();

int main(int argc, char* args[]) {
    if(!initSDL()){
        return 1;
    }

    SDL_Event e;
    bool running = true;
    bool runOnce = false;

    Bulb bulb = (Bulb){1, -3, (Vec3f){0,0,-5}};

    while(running){
        while( SDL_PollEvent( &e ) != 0 ){
            if(e.type == SDL_QUIT){
                running = false;
            }
        }

        SDL_FillRect(surface, NULL, SDL_MapRGB(surface->format, 0x00, 0x00, 0x00));
        if(!runOnce){
            SDL_UpdateWindowSurface(window);
        }
        
        renderBulb(bulb, window, surface, WIDTH, HEIGHT, runOnce);

        if(runOnce){
            SDL_UpdateWindowSurface(window);
        }
        else{
            runOnce = true;
        }

        SDL_Delay(10);
    }

	SDL_DestroyWindow(window);
	SDL_Quit();
    
	return 0;
}

bool initSDL(){
	if(SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("SDL could not initialize. SDL_Error: %s\n", SDL_GetError());
        return false;
	}
	window = SDL_CreateWindow("Ray trace", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	if(window == NULL) {
		printf("Window could not be created. SDL_Error: %s\n", SDL_GetError());
        return false;
	}
    surface = SDL_GetWindowSurface(window);
    return true;
}
